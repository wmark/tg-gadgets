import os.path
import pkg_resources

__all__ = ['FlashClock', 'FlashClockDesc']

from turbogears.widgets import Widget, register_static_directory
from turbogears.widgets.base import CoreWD

swf_dir = pkg_resources.resource_filename(__name__, os.path.join("static", "swf"))
register_static_directory("tggadgets.swf", swf_dir)

class FlashClock(Widget):
    template        = """
    <object type="application/x-shockwave-flash" data="/tg_widgets/tggadgets.swf/${clock}.swf"
            width="${width}" height="${height}" id="${id}">
    <param name="movie" value="/tg_widgets/tggadgets.swf/${clock}.swf" />
    <param name="WMode" value="Transparent" />
    </object>
    """
    params          = ['id', 'clock', 'width', 'height']
    params_doc      = {'id': 'The CSS id for the clock to be inserted',
                       'clock' : 'Identifier of the clock; One of ' \
                                 'clock[1-12], 7a, 7desetembro, bussola, catclock, desenho, glow, handclock, horadolanche, mickeyclock, ' \
                                 'noite, pipa, relog, rolex, roman, samclock, timedimension, timeline'}
    id              = 'flash_clock'
    clock           = 'clock7'
    width           = 160
    height          = 160

class FlashClockDesc(CoreWD):
    name = "Flash Clock"
    for_widget = FlashClock("flash_clock")
