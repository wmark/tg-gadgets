# Release information about TGGadgets

version = "0.6"

description = "A collection of several widgets to be considered as gadgets."
# long_description = "More description about your plan"
author = "W-Mark Kubacki"
email = "wmark.tggadgets@hurrikane.de"
copyright = "2007 - public domain"

# if it's open source, you might want to specify these
# url = "http://yourcool.site/"
download_url = "http://static.ossdl.de/tgwidgets/downloads/"
license = "public domain"
